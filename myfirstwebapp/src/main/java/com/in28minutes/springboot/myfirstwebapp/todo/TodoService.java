package com.in28minutes.springboot.myfirstwebapp.todo;

import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoService {
    private final List<Todo> todos = new ArrayList<>();

    private int todosCount = 0;

    @PostConstruct
    private void initialize() {
        addTodo("mihira", "Learn AWS", LocalDate.of(2023, 12,14), false);
        addTodo("mihira", "Learn DevOps", LocalDate.of(2024, 6,14), false);
        addTodo("mihira", "Learn Full Stack Development", LocalDate.of(2024, 12,14), false);
    }

    public List<Todo> findByUsername(String username) {
        return todos.stream().filter(todo -> todo.getUsername().equalsIgnoreCase(username)).toList();
    }

    public void addTodo(String username, String description, LocalDate targetDate, boolean done) {
        Todo todo = new Todo(++todosCount, username, description, targetDate, done);
        todos.add(todo);
    }

    public void deleteTodo(int id) {
        todos.removeIf(todo -> todo.getId() == id);
    }

    public Optional<Todo> findById(int id) {
        return todos.stream().filter(todo -> todo.getId() == id).findFirst();
    }

    public void updateTodo(Todo todo) {
        deleteTodo(todo.getId());
        todos.add(todo);
    }
}
;