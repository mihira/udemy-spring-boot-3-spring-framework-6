insert into todo (id, username, description, target_date, done)
values(10001, 'mihira', 'Get AWS Certified', current_date(), false);

insert into todo (id, username, description, target_date, done)
values(10002, 'mihira', 'Get Java Certified', current_date(), false);

insert into todo (id, username, description, target_date, done)
values(10003, 'mihira', 'Get Spring Boot Certified', current_date(), false);