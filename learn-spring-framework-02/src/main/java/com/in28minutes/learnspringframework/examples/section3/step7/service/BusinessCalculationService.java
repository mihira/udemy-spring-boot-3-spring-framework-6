package com.in28minutes.learnspringframework.examples.section3.step7.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;

//@Component
@Service
public class BusinessCalculationService {
    private final DataService dataService;

    public BusinessCalculationService(DataService dataService) {
        System.out.println("Constructor injection - BusinessCalculationService with " + dataService);
        this.dataService = dataService;
    }

    public int findMax() {
        return Arrays.stream(this.dataService.retrieveData()).max().orElse(0);
    }
}
