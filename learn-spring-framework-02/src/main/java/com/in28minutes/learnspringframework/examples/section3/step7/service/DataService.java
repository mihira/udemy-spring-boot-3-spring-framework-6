package com.in28minutes.learnspringframework.examples.section3.step7.service;

public interface DataService {
    int[] retrieveData();
}
