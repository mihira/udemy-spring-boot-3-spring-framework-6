package com.in28minutes.learnspringframework.examples.section3.step7;

import com.in28minutes.learnspringframework.examples.section3.step7.service.BusinessCalculationService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class BusinessCalculationApplication {

    public static void main(String[] args) {

        try(var context = new AnnotationConfigApplicationContext(BusinessCalculationApplication.class)) {

            System.out.println("Max number: " + context.getBean(BusinessCalculationService.class).findMax());

        }

    }

}
