package com.in28minutes.rest.webservices.restfulwebservices.versioning;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersioningPersonController {

    @GetMapping("/v1/person")
    public PersonV1 getPersonV1() {
        return new PersonV1("James Bond");
    }

    @GetMapping("/v2/person")
    public PersonV2 getPersonV2() {
        return new PersonV2(new Name("James", "Bond"));
    }

    @GetMapping(path = "/person", params = "version=1")
    public PersonV1 getPersonV1RequestParam() {
        return new PersonV1("James Bond");
    }

    @GetMapping(path = "/person", params = "version=2")
    public PersonV2 getPersonV2RequestParam() {
        return new PersonV2(new Name("James", "Bond"));
    }

    // curl -i http://localhost:8080/person/header --header 'X-API-VERSION: 1'
    @GetMapping(path = "/person/header", headers = "X-API-VERSION=1")
    public PersonV1 getPersonV1HeaderParam() {
        return new PersonV1("James Bond");
    }

    // curl -i http://localhost:8080/person/header --header 'X-API-VERSION: 2'
    @GetMapping(path = "/person/header", headers = "X-API-VERSION=2")
    public PersonV2 getPersonV2HeaderParam() {
        return new PersonV2(new Name("James", "Bond"));
    }

    // curl -i http://localhost:8080/person/accept --header 'Accept: application/vnd.company.app-v1+json'
    @GetMapping(path = "/person/accept", produces = "application/vnd.company.app-v1+json")
    public PersonV1 getPersonV1AcceptHeader() {
        return new PersonV1("James Bond");
    }

    // curl -i http://localhost:8080/person/accept --header 'Accept: application/vnd.company.app-v2+json'
    @GetMapping(path = "/person/accept", produces = "application/vnd.company.app-v2+json")
    public PersonV2 getPersonV2AcceptHeader() {
        return new PersonV2(new Name("James", "Bond"));
    }
}
