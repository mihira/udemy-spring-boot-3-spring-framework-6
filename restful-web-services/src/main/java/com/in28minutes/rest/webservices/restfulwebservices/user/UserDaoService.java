package com.in28minutes.rest.webservices.restfulwebservices.user;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserDaoService {

    //JPA/Hibernate -> Database
    // UserDaoService -> Static list

    private static final List<User> USERS = new ArrayList<>();
    private static int ID = 1;

    static {
        USERS.add(new User(ID++, "Mihira", LocalDate.of(1989,1,11)));
        USERS.add(new User(ID++, "Kikuxki", LocalDate.of(1996,4,21)));
        USERS.add(new User(ID++, "Rudi", LocalDate.of(2015,6,15)));
    }

    public List<User> findAll() {
        return USERS;
    }

    public Optional<User> findById(int userId) {
        return USERS.stream().filter(user -> user.getId().equals(userId)).findFirst();
    }

    public User save(User user) {
        user.setId(ID++);
        USERS.add(user);
        return user;
    }

    public void deleteById(int userId) {
        USERS.removeIf(user -> user.getId().equals(userId));
    }
}
