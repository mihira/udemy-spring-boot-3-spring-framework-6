insert into user_details(id, birth_date, name)
values (100001, current_date(), 'Mihira');

insert into user_details(id, birth_date, name)
values (100002, current_date(), 'Kikuxki');

insert into user_details(id, birth_date, name)
values (100003, current_date(), 'Rudi');


-- post
insert into post(id, description, user_id)
values(200001, 'I want to learn Spring Boot', 100001);

insert into post(id, description, user_id)
values(200002, 'I want to learn React', 100001);

insert into post(id, description, user_id)
values(200003, 'I want to find a job', 100002);

insert into post(id, description, user_id)
values(200004, 'I want to learn to walk', 100003);

