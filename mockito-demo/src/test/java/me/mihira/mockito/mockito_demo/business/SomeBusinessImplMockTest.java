package me.mihira.mockito.mockito_demo.business;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SomeBusinessImplMockTest {

    @Mock
    private DataService dataServiceMock;

    @InjectMocks
    private SomeBusinessImpl someBusiness;

    @Test
    void findTheGreatestFromAllData_basicScenario() {
        when(this.dataServiceMock.retrieveAllData()).thenReturn(new int[]{5, 15, 25});

        int result = this.someBusiness.findTheGreatestFromAllData();

        assertEquals(25, result);
    }

    @Test
    void findTheGreatestFromAllData_oneValue() {
        when(this.dataServiceMock.retrieveAllData()).thenReturn(new int[]{35});

        int result = this.someBusiness.findTheGreatestFromAllData();

        assertEquals(35, result);
    }
}