package me.mihira.mockito.mockito_demo.business;

public class SomeBusinessImpl {

    private DataService dataService;

    SomeBusinessImpl(DataService dataService) {
        this.dataService = dataService;
    }

    public int findTheGreatestFromAllData()
    {
        int[] data = this.dataService.retrieveAllData();

        int greatest = Integer.MIN_VALUE;
        for (int value : data) {
            if (value > greatest) {
                greatest = value;
            }
        }
        return greatest;
    }
}

interface DataService {
    int[] retrieveAllData();
}
