package me.mihira.learn_spring_aop.aopexample.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
@Aspect
public class LoggingAspect {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    // Pointcut - When?
    // execution(* PACKAGE.*.*(..))
    @Before("me.mihira.learn_spring_aop.aopexample.aspects.CommonPointcutConfig.allServiceConfigUsingBean()")
    public void logMethodCallBeforeExecution(JoinPoint joinPoint) {
        logger.info("Before Aspect - {} called with arguments: {}",
                joinPoint, joinPoint.getArgs());
    }

    @After("me.mihira.learn_spring_aop.aopexample.aspects.CommonPointcutConfig.businessPackageConfig()")
    public void logMethodCallAfterExecution(JoinPoint joinPoint) {
        logger.info("After Aspect - {} has executed", joinPoint);
    }

    @AfterThrowing(
            pointcut = "me.mihira.learn_spring_aop.aopexample.aspects.CommonPointcutConfig.businessAndDataPackageConfig()",
            throwing = "exception"
    )
    public void logMethodCallAfterException(JoinPoint joinPoint, Exception exception) {
        logger.info("AfterThrowing Aspect - {} has thrown an exception.", joinPoint, exception);
    }

    @AfterReturning(
            pointcut = "me.mihira.learn_spring_aop.aopexample.aspects.CommonPointcutConfig.dataPackageConfig()",
            returning = "returnValue"
    )
    public void logMethodCallAfterReturning(JoinPoint joinPoint, Object returnValue) {
        logger.info("AfterReturning Aspect - {} is returnValue {}", joinPoint, returnValue);
    }
}
