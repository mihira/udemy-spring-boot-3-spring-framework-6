package me.mihira.learn_spring_aop.aopexample.data;

import me.mihira.learn_spring_aop.aopexample.annotations.TrackTime;
import org.springframework.stereotype.Repository;

@Repository
public class DataService2 {

    @TrackTime
    public int[] retrieveData() {

        try {
            Thread.sleep(19);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return new int[] { 111, 222, 333, 444, 555 };
    }

}
