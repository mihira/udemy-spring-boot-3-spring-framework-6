package me.mihira.learn_spring_aop.aopexample.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
@Aspect
public class PerformanceTrackingAspect {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Around("me.mihira.learn_spring_aop.aopexample.aspects.CommonPointcutConfig.trackTimeAnnotation()")
    public Object findExecutionTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        //Start a timer
        var start = System.currentTimeMillis();

        //Execute the method
        Object returnValue = proceedingJoinPoint.proceed();

        // Stop the timer
        var stop = System.currentTimeMillis();

        var duration = stop - start;
        logger.info("Around Aspect - {} method executed in {}ms", proceedingJoinPoint, duration);

        return returnValue;
    }
}
