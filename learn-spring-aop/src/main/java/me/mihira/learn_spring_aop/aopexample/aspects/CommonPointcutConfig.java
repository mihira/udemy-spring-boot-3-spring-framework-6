package me.mihira.learn_spring_aop.aopexample.aspects;

import org.aspectj.lang.annotation.Pointcut;

public class CommonPointcutConfig {

    @Pointcut("execution(* me.mihira.learn_spring_aop.aopexample.*.*.*(..))")
    public void businessAndDataPackageConfig() {}

    @Pointcut("execution(* me.mihira.learn_spring_aop.aopexample.data.*.*(..))")
    public void dataPackageConfig() {}

    @Pointcut("execution(* me.mihira.learn_spring_aop.aopexample.business.*.*(..))")
    public void businessPackageConfig() {}

    @Pointcut("bean(*Service*)")
    public void allServiceConfigUsingBean() {}

    @Pointcut("@annotation(me.mihira.learn_spring_aop.aopexample.annotations.TrackTime)")
    public void trackTimeAnnotation() {}

}
