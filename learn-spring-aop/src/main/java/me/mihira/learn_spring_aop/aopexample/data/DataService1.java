package me.mihira.learn_spring_aop.aopexample.data;

import org.springframework.stereotype.Repository;

@Repository
public class DataService1 {

    public int[] retrieveData() {

        try {
            Thread.sleep(32);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return new int[] { 11, 22, 33, 44, 55 };
    }

}
