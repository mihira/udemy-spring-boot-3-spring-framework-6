1. Docker Introduction
2. Docker Fundamentals
3. How docker works
	1. `-p hostPort:containerPort` this maps a port from the machine running the image to a port into the container
	2. `-d` detached mode
4. Docker terminology
5. Dockerfile
	1. Download [repository](https://codeload.github.com/in28minutes/master-spring-and-spring-boot/zip/refs/heads/main)
	2. open 83-docker/hello-world-java project
	3. Create `Dockerfile`
	4. `mvn clean install` to generate the jar file
	5.  `docker build -t in28min/hello-world-docker:v1 .`
	6. `docker image list`
	7. `docker run -d -p 5000:5000 in28min/hello-world-docker:v1`
	8. Then application will be visible at localhost:5000
6. Multi Stage Dockerfile 
	1. Copy Dockerfile 2 into Dockerfile
	2.  `docker build -t in28min/hello-world-docker:v2 .`
7. Optimizing Dockerfile
	1. Docker layering
		1. Every line on Dockerfile introuces a layer
		2. Docker caches every layer and tries to reuse it
			1. If nothing changes up to the point of the line executes it will not execute again
8. Building Image with Spring Boot Maven plugin
	1. `mvn spring-boot:build-image`
		1. It will create a very efficient image
		2. At least java 17
		3. It builds and creates images very fast
9. Docker review
