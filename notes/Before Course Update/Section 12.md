1. Extract project from [here](https://github.com/in28minutes/master-spring-and-spring-boot/blob/main/13-full-stack/99-reuse/01-rest-api-starting-code.zip)
2. Call Spring Boot Hello World REST API from React component using axios
	1. Install axios `npm install axios`
	2. `import axios from 'axios'`
	3.  `npm start`
3. Set up WebMvcConfigurer Bean to allow Cross Origin Requests from localhost:3000
4. Simple use of useState to display hello world call response
5. Move axios call to a separate file ApiService
6. Small code refactoring and calling passing a path variable
7. Implement TodoController and add API call to return todos by username
	1. `http://localhost:8080/users/mihira/todos` return all todos for `mihira`
8. Implement state to display todos from the api call from step 7
9. Implement retrieve todo and delete todo on the backend
10. Implement Delete todo button on frontend
11. Remove hardcoded username by using authContext to propagate username
12. Creating TodoComponent to manage existing Todos
13. Use Formik and Moment libs to manage form html components
	1. `npm install formik`
	2. `npm install moment
	3. Use `<Formik>`, `<Form>` and `<Field>` components to display todo values
14. Using Formik `validate` to validate fields on submit
15. Add PUT and POST to update and create a new todos
	1. `curl -X PUT -i http://localhost:8080/users/mihira/todos/1 -d '{"id":1, "username": "mihira", "description": "Change description", "targetDate": "2033-11-10", "done": false}' --header 'Content-Type: application/json'`
	2. `curl -X POST -i http://localhost:8080/users/mihira/todos -d '{"username": "mihira", "description": "Learn to Dance", "targetDate": "2033-11-10", "done": false}' --header 'Content-Type: application/json'`
16. Call api PUT endpoint to update todo from React front-end
17. Call api POST to create a new todo from React front-end
18. Integrate Spring Boot Security with a basic filter chain
19. Introduce Authorization header on hello-world call
20. Some refactoring for new Spring version and allow Options requests to go through
21. Introduce call to basic authentication service when login is performed on front-end
22. Use async and await to invoke Basic Auth API call
23. Set and read Basic Auth Token into and from AuthContext
24. Centralize ApiClient and set Basic Auth header on all api calls
25. Integrate JWT into back-end side
	1. Copy classes from [here](https://github.com/in28minutes/master-spring-and-spring-boot/blob/main/13-full-stack/99-reuse/02-spring-security-jwt.md) to package `com.in28minutes.rest.webservices.restfulwebservices.jwt`
	2. Add missing dependency `<dependency><groupId>org.springframework.boot</groupId>	<artifactId>spring-boot-starter-oauth2-resource-server</artifactId></dependency>`
	3. Comment `.requestMatchers(PathRequest.toH2Console()).permitAll()` line inside `JwtSecurityConfig:securityFilterChain`
	4. Change `User.withUsername("mihira")` line in the same class
	5. Run back-end and call `curl -X POST -i http://localhost:8080/authenticate -d '{"username": "mihira", "password": "dummy"}' --header 'Content-Type: application/json'`
	6. Store the token json response somewhere
	7. Send a GET request using heaver `Authorization: Bearer <JWT Token>`
	8.  `curl -X GET -i http://localhost:8080/users/mihira/todos --header 'Accept: application/json' --header 'Authorization: Bearer eyJraWQiOiI0M2RiODEzOS1iNWQ2LTRhMjQtOGQ4My1hMDM3NWMxY2RlOTgiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzZWxmIiwic3ViIjoibWloaXJhIiwiZXhwIjoxNzIyMDExODIwLCJpYXQiOjE3MjIwMDY0MjAsInNjb3BlIjoiUk9MRV9VU0VSIn0.n7DJ8obSAcbWEG93IQGvf4xc9I1Cmt4mANr4S7bL8RORX8Ck3FAZGDIUAItoe8rSVMIeeq4C_NT1Adng4gmKYJyRagd28EAAhL6EzpuNacxO9cxECuOcvuRTHwolDpCK6F5aNyvRDBAvfJBCf3K09cKYGxluKaBaFRqE2LIdEjdHQx4v7X2FqxnzEuJqbQURxsjtqOa25P_GiM508H7avkdkXSr6WM-zKeGNnmicMHjLs_duiBtAdD6uPLeo5r7OhFKJGxH6hZkPDczpDpZr1Zp9yadekp4K0J1UqOseyaaBA1-S-xQbsyKEj3J0MUZebVwj81gEMJjrl_qIbe70kg'`
26. Integrate JWT authentication into Front-end