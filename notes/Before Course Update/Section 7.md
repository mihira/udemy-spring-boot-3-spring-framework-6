- Step 0 - Introduction to building Web App with Spring Boot
	- A TODO list management app
- Step 1 - Initialize Spring Boot project
- Step 2 - Review important files of the project
- Step 3 - Create hello world
	- `@RestController` ->`@Controller` + `@ResponseBody`
		- When you use `@RestController` all methods in the class will have a `@ResponseBody` annotation
- Step 4.1 - continue further with a html page with Strings
- Step 4.2 - Covering the [Step by Step changes guide](https://github.com/in28minutes/master-spring-and-spring-boot/blob/main/11-web-application/Step05.md)
- Step 5
	- Returning html using a view with apache jasper and jsp files
- Step 6 - Introduce `/login`, `LoginController` and `login.jsp`
- Step 7 - Quick Overview
	- Some HTML and Browser investigations
	- HTTP Requests investigations
- Step 8 - Query parameters
- Step 9 - Logging
	- `logging.level.the.package.you.want.to.log=debug` you can control the logging level produced at a specific package
- Step 10 - History of JSPs 
	- Model 1 Arch. - Only JSPs
	- Model 2 Arch. - Servlet, Model, View
		- + FrontController
	- Spring MVC Front Controller - Dispatcher Servlet
		- How it works step by step
			1. Identify -> Executes Controller
			2. Identify -> Executes View
- Step 11 - Login form
	- POST vs GET
- Step 12 - Displaying login credentials
- Step 13 - Hard-coded Authentication Service
- Step 14 - Getting started with Todo Service
	- Todo Bean
	- TodoService with static data
- Step 15 - Initial Todos Page
- Step 16 - Session vs Model vs Request
	- Share data cross Controllers using `@SessionAttributes`
- Step 17 - Adding JSTL
	- for IntelliJ the glassfish implementation is found under `org.glassfish.web:jakarta.servlet.jsp.jstl`
	- [JSTL core](https://docs.oracle.com/javaee/5/jstl/1.1/docs/tlddocs/c/tld-summary.html)
- Step 18 - Install Bootstrap
```xml
<dependency>  
    <groupId>org.webjars</groupId>  
    <artifactId>bootstrap</artifactId>  
    <version>5.3.2</version>  
</dependency>  
<dependency>  
    <groupId>org.webjars</groupId>  
    <artifactId>jquery</artifactId>  
    <version>3.7.1</version>  
</dependency>
```
```html
    <head>  
       <link href="webjars/bootstrap/5.3.2/css/bootstrap.min.css" rel="stylesheet" />  
    </head>  
    <body>  
		<script src="webjars/bootstrap/5.3.2/js/bootstrap.min.js"></script>  
		<script src="webjars/jquery/3.7.1/jquery.min.js"></script>
	</body>
```
- Step 19 - Improve look and feel using bootstrap
- Step 20 - Create Add Todo page
	- `return "redirect:path"` to redirect to another url path
- Step 21 - Create Todo inside TodoService
- Step 22 - Validations with Spring Boot
	- 2-way binding (`todo.jsp` with `TodoController.java`)
- Step 23 - Add Validations to Bean and Display Errors
- Step 24 - Delete Todo Feature
- Step 25 - Update Todo Feature
- Step 26 - Continue Update Todo Feature
- Step 27 - Integrate Bootstrap datepicker
- Step 28 - JSP Fragments and Navigation Bar
- Step 29 - Preparing for Spring Security
- Step 30 - Set up Spring Security
- Step 31 - Configure Spring Security with `PasswordEncoder`
- Step 32 - Removing Hardcoded user id
- Step 33 - Adding more users
- Step 34 - Set up H2 Database
- Step 35 - Configure `SecurityFilterChain` to be able to access H2 Console
- Step 36 - Making Todo and Entity and init table on system boot
	- `data.sql` with insert commands
	- `spring.jpa.defer-datasource-initialization=true` to execute after initialization
- Step 37 - 38 - TodoRepository to connect to H2 and Replace usage from TodoService in TodoController
- Step 39 + 40 - Installing Docker MySQL instance
- Step 41 - Todo App with MySQL
	- `data.sql` is not executed when you are connecting to a real database
	- `docker exec -it mysql mysql -u todos-user -p` to connect to docker mysql