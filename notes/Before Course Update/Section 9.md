1. Introduction
4. Install VS Code
5. Install nodejs and npm
		- `npm init`
		- `package.json` dependencies definitions
6. React Introduction
7. Explore nodejs commands
	- `npm start`
	- `npm test`
	- `npm run build`
8. Create React app and playing around with vs code
9. Explore React App folder structure
10. Intro to React Components
	- Name of the component *must* start with a Capital letter
11. Function vs Class components
12. State in React - useState hook
	- Each file `.js`  is a module. Ex.: `App.js`
	- Since React evolved it is recommended to use function components only
13. Explore JSX
	- It is stricter than html, only 1 element returned
	- [Babel](https://babeljs.io/repl): Converts JSX into JS
	- Recommended to wrap JSX into parentesis `()` inside components
	- Instead of `class` we should use `className`
14. JavaScript best practices
	- One file per Component
	- Default import: `import Component from ...` will always import the default export
	- Named import: `import {Component} from ...` will import the named export
15. Explore JavaScript further