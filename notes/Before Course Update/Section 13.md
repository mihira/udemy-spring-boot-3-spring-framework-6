1. Add H2Console connection to our project
2. Set up tables and add initial vaues in H2db
3. Implement JpaRepository for Todo object and use it to connect api with h2db
4. Small changes on createTodo method
5. Connect back-end to mysql database
	1. Follow instructions [here](https://github.com/in28minutes/master-spring-and-spring-boot/tree/main/13-full-stack/02-rest-api)
	2. Create docker mysql container: `docker run --detach --env MYSQL_ROOT_PASSWORD=dummypassword --env MYSQL_USER=todos-user --env MYSQL_PASSWORD=dummytodos --env MYSQL_DATABASE=todos --name mysql --publish 3306:3306 mysql:8-oracle`