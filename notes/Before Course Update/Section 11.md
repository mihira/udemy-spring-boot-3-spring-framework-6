1. Intro to the section goals
2. Start `TodoApp`
3. 
	1. Add css
	2. use `useState` to bound username field
	3. do the same for password field
4. Use previously states to update ui elements based on state change
5. Simplify the error message components using `{booleanVar && <div />}`
6. 
	1. `npm install react-router-dom`
	2. use `useNavigate` hook to forward navigation
	3. creating the paths to specific components
7. Small changes on the welcome page and added a 404
8. Use useParams to handle url parameters
9. Create a simple list of todos
10. Introduction of Link element to replace `<a>` tag for single page applications
11. Create Header, Footer and Logout components
12. Install bootstrap
	1. `npm install bootstrap`
	2. Add `import 'bootstrap/dist/css/bootstrap.min.css';` to `index.js`
	3. `npm start`
13. Use bootstrap to style header and footer keeping a single page application
14. Refactor to split components into single files
15. How to share state between multiple components using createContext and useContext
16. Small refactoring and verify that state is share when context is modified
17. Share isAuthenticated state shared between components to manage login/logout
18. Refactor code to centralize authentication functions into `AuthContext.js`
19. Some small function to protect components to be displayed based on the isAuthenticated state