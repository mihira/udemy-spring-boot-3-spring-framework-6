1. Intro
2. Getting Started with React Application Counter
3. Add `Counter.css` inside Counter component
4. Adding state to Counter - exploring React State with useState hook
5. Explaining what is happening in the background
6. React Props - Setting Counter increment value
7. Exploring propTypes, defaultProps
8. Refactoring Counter and CounterButton
9. React State Up - Calling parent Component Methods
10. Explore React Developer Tools [extension](https://addons.mozilla.org/en-US/firefox/addon/react-devtools/)
11. Add Reset Button to Counter
12. Refactor and cleanup code
	- Call parent function directly using arrow function `onClick={() => incrementFun(by)}`
