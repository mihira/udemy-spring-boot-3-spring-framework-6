1. Understanding Security Fundamentals
	1. Authentication: Who?
	2. Authorization: Permissions for Authenticated users
2. Important Security Principles
	1. 6 Principles:
		1. Trust Nothing
		2. Assign Least Privileges
		3. Have Complete Mediation (well implemented security filter)
		4. Have Defence In Depth
		5. Have Economy of Mechanism
		6. Ensure Openness Of Design
3. Spring Security
	1. Protects everything by default
	2. Request(Rq) -> Dispatcher Servlet(DS)-> Controllers
		1. Spring Security enters between Rq and DS
4. Introduce Default Spring Security Configuration
	1. Executes a series of filters providing these features:
		1. Authentication
		2. Authorization
		3. Cross-Origin Resource Sharing (CORS) CorsFilter
		4. Cross Site Request Forgery (CSRF) CsrfFilter
		5. Login / Logout pages
		6. Translation exceptions into Http Responses
	2. They execute into specific order
		1. Basic Checks: CORS, CSRF
		2. Authentication
		3. Authorization
5. Create Project for Spring security
	1. Access https://start.spring.io/
	2. Add Spring Web and Spring Security dependencies
6. Form Authentication explanation
	1. All urls are accessible with credentials `user` and password present in logs
	2. It will add `JSESSIONID` cookie in request headers
	3. `/logout` to perform logout
7. Basic Authentication explanation
	1. I am using [RESTED Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/rested/?src=github)
	2. Add `spring.security.user.name` and `spring.security.user.password` into `application.properties` to set up default user password
8. Cross-Site Request Forgery (CSRF)
	1. Get mappings works fine
	2. Post mappings fails
		1. Sprint Security protects from CSRF by default
9. Create endpoint to return CSRF token
	1. Add token returned by `/csrf-token` into POST header `X-Csrf-Token`
10. Disable CSRF in Spring Secutiry Configuration
	1. Copy `defaultSecurityFilterChain` from `SpringBootWebSecurityConfiguration`
	2. Create `BasicAuthSecurityConfiguration` and modify copied security filter chain
	3. Add `http.csrf(AbstractHttpConfigurer::disable);` to disable csrf
11. Cross-Origin Resource Sharing (CORS)
	1. Can be set on Global Configuration
		1. Via `addCorsMappings` callback in `WebMvcConfigurer`
	2. Can be set on Local Configuration
		1. Via `@CrossOrigin` - from all origins
		2. `@CrossOrigin(origins = "https://mihira.me")` - Allow from specific origin
12. Storing User Credentials in memory
13. Storing User Credentials using JDBC
14. Encoding vs Hashing vs Encryption
15. Storing Bcrypt Encoded Passwords
	1. PasswordEncoded - Bad naming, they are all performing hashing
16. Intro to JWT Authentication
	1. Symmetric Key Encryption: Same key used for encryption and decryption
	2. Asymmetric Key Encryption: Different keys to encrypt and de-crypt (public and private keys)