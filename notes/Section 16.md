1. Introduction to Gradle
2. Start a gradle project
	1. Access https://start.spring.io/
	2. Gradle + Latest version of Spring (3.3.3)
	3. Generate and open project on IntelliJ
	4. Run project
3. Explore `build.gradle` and `settings.gradle` files
4. Explore Gradle Plugins for Java and Spring Boot
	1. `java` plugin provides compilation, testing and bundling
	2. Dependency Managent
	3. Spring Boot Gradle Plugin
5. Maven vs Gradle
	1. Spring Framework uses Gradle since 2012
	2. Spring Boot uses Gradle since 2020
	3. Spring Cloud uses Maven with no plans to switch
	4. Maven: Familiar, Simple and Restrictive
	5. Gradle: Faster build times, less verbose and flexible (but can become complex)