1. Introduction to Aspect Oriented Programming
2. What is AOP?
	1. Cross Cutting Concern
	2. Poular AOP Frameoworks
		1. Spring AOP
		2. AspectJ
3. Create a Spring project using https://start.spring.io/
	1. Pick Gradle and latest Spring version (3.3.3 now)
	2. Generate and open the project in IntelliJ
4. Create some simple Service and Repository to be called in main application
5. Introduce Spring AOP in the project
	1. Add `implementation 'org.springframework.boot:spring-boot-starter-aop'` to dependencies
	2. Use tags `@Aspect` and `@Before` to capture method calls using Pointcut description
	3. `@Before("execution(* PACKAGE.*.*(..))"`
	4. `JoinPoint` is the parameter of the method information being intercepect
6. Explanation about AOP Terminology
	1. Advice - Logging, Authentication
	2. Pointcut - Identifies method calls to be intercepted
	3. Aspect - Advice + Pointcut
	4. Weaver - Framework implementing AOP: AspectJ or Spring AOP
7. Exploring other AOP Annotations
	1. `@Before` - Do something before it is called
	2. `@After` - Do something after it is called, regardless of the result
	3. `@AfterThrowing` - Do something after an exception has happened
		1. Can access exception thrown with `throwing` property in tag
	4. `@AfterReturning` - Do something after returning successfully
		1. Can access returning value with `returning` property in tag
8. Explore Around AOP annotation with a Timer class
9. AOP Best Practice - Common Pointcut Definitions
	1. Define a common Pointcut config, in case your package structure changes
	2. We can also define beans that match some pattern as a Pointcut
10. Creating Track Time Annotation
11. Review of the Section