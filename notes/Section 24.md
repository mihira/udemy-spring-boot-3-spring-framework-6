1. Setup empty Spring project in https://start.spring.io
	1. `spring-boot-starter-test` includes mockito into project
	2. Add sample code to test with mockito
2. Cover problems with Stubs
3. Introduction to Mocks (nothing new learned)
4. Introduce @Mock @InjectMocks annotations
	1. Add `@ExtendWith(MockitoExtension.class)` into the test class
	2. Add `@Mock` into objects to be mocked
	3. Add `@InjectMocks` into objects which the mocks have to be injected
5. Some very simple method mocks