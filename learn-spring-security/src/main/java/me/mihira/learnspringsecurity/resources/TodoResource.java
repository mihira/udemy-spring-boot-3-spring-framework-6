package me.mihira.learnspringsecurity.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoResource {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final List<Todo> TODO_LIST = List.of(
            new Todo("mihira", "Learn AWS"),
            new Todo("mihira", "Learn Docker")
    );

    @GetMapping("/todos")
    public List<Todo> retrieveAllTodos() {
        return TODO_LIST;
    }

    @GetMapping("/users/{username}/todos")
    public List<Todo> retrieveTodosForUser(@PathVariable String username) {
        return TODO_LIST.stream()
                .filter(todo -> username.equals(todo.username))
                .toList();
    }

    @PostMapping("/users/{username}/todos")
    public void createTodoForUser(@PathVariable String username, @RequestBody Todo todo) {
        logger.info("Create {} for {}", todo, username);
    }

    public record Todo (String username, String description) {}

}
