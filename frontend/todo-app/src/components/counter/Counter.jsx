import "./Counter.css"
import { useState } from 'react'
import CounterButton from "./CounterButton";

export default function Counter() {
    const [count, setCount] = useState(0);

    function increment(by) {
        setCount(count + by);
    }

    function reset() {
        setCount(0);
    }

    return (
        <>
            <CounterButton incrementFun={increment}/>
            <CounterButton by={2} incrementFun={increment}/>
            <CounterButton by={5} incrementFun={increment}/>
            <div className="totalCount count">{count}</div>
            <button className="resetButton" onClick={reset}>Reset</button>
        </>
    )
}