import {PropTypes} from 'prop-types'

export default function CounterButton({by, incrementFun}) {

    return (
        <div className="CounterButton">
            <div>
                <button className="counterButton"
                    onClick={() => incrementFun(-by)}
                >-{by}</button>
                <button className="counterButton"
                    onClick={() => incrementFun(by)}
                >+{by}</button>
            </div>
        </div>
    );
}

CounterButton.propTypes =  {
    by: PropTypes.number
}

CounterButton.defaultProps =  {
    by: 1
}