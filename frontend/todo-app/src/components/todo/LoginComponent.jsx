import {useState} from 'react'
import { useNavigate } from 'react-router-dom'
import { useAuth } from './security/AuthContext'

export default function LoginComponent() {

    const [username, setUsername] = useState('mihira')
    const [password, setPassword] = useState('')
    const [showFail, setShowFail] = useState(false)
    const navigate = useNavigate()
    const authContext = useAuth()

    function handleUsernameChange(event) {
        setUsername(event.target.value)
    }

    function handlePasswordChange(event) {
        setPassword(event.target.value)
    }

    async function handleSubmit() {
        if(await authContext.login(username, password)) {
            navigate(`/welcome/${username}`)
        }
        else {
            setShowFail(true)
        }
    }

    return (
        <div className="Login">
            {showFail && <div className="fail" >Authentication Failed! Please check your credentials.</div>}
            <div className="LoginForm">
                <h1>Please Login!</h1>
                <div>
                    <label>User Name</label>
                    <input type="text" name="username" value={username} onChange={handleUsernameChange} />
                </div>
                <div>
                    <label>Password</label>
                    <input type="password" name="password" value={password} onChange={handlePasswordChange} />
                </div>
                <div>
                    <button name="login" onClick={handleSubmit}>Login</button>
                </div>
            </div>
        </div>
    )
}