import { Link, useParams } from 'react-router-dom'
import {useState} from 'react'
import { retrieveHelloWorldPathVariable } from './api/HelloWorldApiService'
import { useAuth } from './security/AuthContext'

export default function WelcomeComponent() {

    const {username} = useParams()

    const [message, setMessage] = useState(null)


    function callHelloWorldRestApi() {
        retrieveHelloWorldPathVariable('mihira')
            .then( (response) => successResponse(response))
            .catch( (error) => errorResponse(error))
            .finally( () => console.debug('cleanup!') )
    }

    function successResponse(response) {
        console.debug(response)
        setMessage(response.data.message)
    }

    function errorResponse(error) {
        console.error(error)
    }

    return (
        <div className="WelcomeComponent">
            <h1>Welcome, {username}!</h1>
            <div>
                Manage your todos -  <Link to="/todos">Go here</Link>
            </div>
            <div>
                <button className="btn btn-success m-5" onClick={callHelloWorldRestApi}>
                    Call Hello World
                </button>
            </div>
            <div className="text-info">{message}</div>
        </div>
    )
}