import { createContext, useContext, useState } from "react";
import { executeJwtAuthenticationService } from "../api/AuthenticationApiService";
import { apiClient } from "../api/ApiClient";

// 1: Create a Context
export const AuthContext = createContext()

export const useAuth = () => useContext(AuthContext)

//2: Share the created context with other components

export default function AuthProvider({ children }) {

    //3: Put some state in the context
    const [isAuthenticated, setAuthenticated] = useState(false)

    const [username, setUsername] = useState(null)

    const [token, setToken] = useState(null)

    async function login(username, password) {

        var isSuccess
        var jwtToken

        try {
            const response = await executeJwtAuthenticationService(username, password)
            isSuccess = response.status===200
            jwtToken = 'Bearer ' + response.data.token
        } catch(error) {
            isSuccess = false
        }

        setAuthenticated(isSuccess)
        if (isSuccess) {
            setUsername(username)
            setAuthorizationToken(jwtToken)
        }
        else {
            logout()
        }
        return isSuccess
    }

    function setAuthorizationToken(baToken) {
        setToken(baToken)
        apiClient.interceptors.request.use(
            (config) => {
                console.log('Intercepting and adding a token')
                config.headers.Authorization = baToken
                return config
            }
        )
    }

    function logout() {
        setAuthenticated(false)
        setUsername(null)
        setToken(null)
    }

    return (
        <AuthContext.Provider value={ { isAuthenticated, login, logout, username, token } }>
            {children}
        </AuthContext.Provider>
    )
}