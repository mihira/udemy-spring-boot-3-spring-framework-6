package com.in28minutes.learnspringframework.game.configuration;

import com.in28minutes.learnspringframework.game.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class GamingConfiguration {

    @Bean
    @Primary
    public GameRunner gameRunner(GamingConsole gamingConsole) {
        //It seems it is getting the Primary GamingConsole.class, which is mario
        return new GameRunner(gamingConsole);
    }

    @Bean
    @Primary
    public GamingConsole mario() {
        return new MarioGame();
    }

    @Bean
    public GamingConsole superContra() {
        return new SuperContraGame();
    }

    @Bean
    public GamingConsole pacMan() {
        return new PacManGame();
    }
}
