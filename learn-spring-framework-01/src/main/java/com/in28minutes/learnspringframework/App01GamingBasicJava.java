package com.in28minutes.learnspringframework;

import com.in28minutes.learnspringframework.game.GameRunner;
import com.in28minutes.learnspringframework.game.configuration.GamingConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App01GamingBasicJava {

    public static void main(String[] args) {

        try(var context = new AnnotationConfigApplicationContext(GamingConfiguration.class)) {
            context.getBean(GameRunner.class).run();
        }

    }

}
